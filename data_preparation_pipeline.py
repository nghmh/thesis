import argparse
import os
import sys
import logging
import time
from pipeline_stages import *
from pipeline_executor import PipelineExecutor

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s.%(msecs)03d %(threadName)s %(message)s',
                        datefmt='%m-%d %H:%M:%S')
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input_file",
                        help="News article data in tsv format", required=True,
                        type=argparse.FileType('r'), nargs='+')
    parser.add_argument("-d", "--debug", help="Write intermediate data to disk \
                        for debugging purposes", action="store_true")
    args = parser.parse_args()

    for file in args.input_file:
        props = vars(args)
        props["input_file"] = file
        pipeline = PipelineExecutor(**props)
        pipeline.set_write_outputs_function(write_results)
        pipeline.add(ReadTSV(input_file=args.input_file,
                             relevant_columns=["id1", "id2", "articleID"]))
        pipeline.add(ApplyWordFrequenciesCutoff(input_file=args.input_file,
                                                cutoff_rate=10))
        pipeline.add(ComputeFrequencyMatrix(input_file=args.input_file))
        pipeline.add(ComputePairwiseFrequencies(input_file=args.input_file))
        pipeline.add(ComputeThetaPWeights(input_file=args.input_file))
        pipeline.add(BuildGraphToolDataframe(input_file=args.input_file))
        pipeline.add(BuildGraphToolGraph(input_file=args.input_file))
        pipeline.run()
