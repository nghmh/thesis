import logging
import pandas as pd
import os
from joblib import Parallel, delayed
from functools import partial
import numpy as np
from graph_tool.all import *

class PipelineStage(object):
    def __init__(self, **kwargs):
        self.properties = kwargs
        self.successor = None
        self.inputs = None
        self.results = None

    def run(self):
        logging.error("Pure virtual function call!")
        raise NotImplementedError

    def set_successor(self, other):
        if not isinstance(other, PipelineStage):
            logging.error("Successor is not a PipelineStage object")
            raise TypeError
        self.successor = other

    def write_results(self):
        logging.error("Pure virtual function call!")
        raise NotImplementedError

    def describe(self):
        logging.error("Pure virtual function call!")
        raise NotImplementedError

    def set_inputs(self, inputs):
        self.inputs = inputs

    def set_outputs(self):
        self.inputs = None
        if self.successor is not None:
            self.successor.set_inputs(self.results)
            self.results = None
        else:
            logging.debug("Nothing to do. Successor not set...")

    def get_results(self):
        return self.results

class ReadTSV(PipelineStage):
    def run(self):
        self.results = pd.read_csv(self.properties["input_file"],
                                sep='\t', header=0)

        if "relevant_columns" in self.properties:
            self.results = self.results.filter(
                self.properties["relevant_columns"], axis=1)
    def describe(self):
        return "Read TSV File '{}'".format(self.properties["input_file"])


    def write_results(self):
        logging.info("ReadTSV File: Nothing to do...")


class ApplyWordFrequenciesCutoff(PipelineStage):
    def run(self):
        df = self.inputs

        id1 = df.groupby(by="id1", as_index=False).agg({'articleID': pd.Series.nunique})
        id2 = df.groupby(by="id2", as_index=False).agg({'articleID': pd.Series.nunique})

        id1.rename(columns={"id1": "id"}, inplace=True)
        id2.rename(columns={"id2": "id"}, inplace=True)
        total = pd.concat([id1, id2], axis=0, ignore_index=True)
        total = total.groupby(by="id", as_index=False).agg({'articleID': pd.Series.sum})

        cutoff_rate = self.properties["cutoff_rate"]
        total = total[total["articleID"] > cutoff_rate]
        relevant_words = total["id"].values.tolist()
        results = df.loc[(df["id1"].isin(relevant_words)) & \
                         (df["id2"].isin(relevant_words))]
        self.results = { "filtered_data" : results,
                         "relevant_words" : relevant_words }


    def write_results(self):
        file_name, _ = os.path.splitext(self.properties["input_file"])
        output_file_name = "{}_filtered_word_freq_gt_{}.xz".format(
            file_name,
            self.properties["cutoff_rate"])
        logging.info("ApplyWordFrequenciesCutoff: Writing results to file " \
                     "'{}'...".format(output_file_name))
        self.results["filtered_data"].to_pickle(output_file_name)


    def describe(self):
        return "Apply word frequency based cutoff (> {})".format(
            self.properties["cutoff_rate"])

class ComputeFrequencyMatrix(PipelineStage):
    @staticmethod
    def compute_frequency(article, original_data, relevant_words):
        result = original_data.loc[(original_data["articleID"] == article),
                                   ['id1', 'id2']]
        array = pd.unique(result.values.ravel('K'))
        tmp = pd.Series(True, index=array, dtype=bool).reindex(
                        index=relevant_words, fill_value=False)
        tmp.rename(article, inplace=True)
        return tmp

    def run(self):
        df = self.inputs["filtered_data"]
        relevant_words = self.inputs["relevant_words"]
        articles = df["articleID"].unique().tolist()
        result = Parallel(n_jobs=-1, verbose=1, backend='threading')(
            map(delayed(
                partial(ComputeFrequencyMatrix.compute_frequency,
                        original_data=df,
                        relevant_words=relevant_words)),
                articles))
        self.results = pd.concat(result, axis=1, copy=False)

    def write_results(self):
        file_name, _ = os.path.splitext(self.properties["input_file"])
        output_file_name = "{}_frequence_matrix.xz".format(file_name)
        logging.info("ComputeFrequencyMatrix: Writing results to file " \
                     "'{}'...".format(output_file_name))
        self.results.to_pickle(output_file_name)

    def describe(self):
        return "Build word in article occurrence matrix for the delta_p method"

class ComputePairwiseFrequencies(PipelineStage):
    @staticmethod
    def compute_cooccurrence_frequencies(word, data):
        """
        @param word: rows of the Dataframe which consists of frequency
                        of occurrence of words in documents.
        @param data: Pandas Dataframe based on which pairwise frequencies are computed.
        @param result: Pandas Dataframe which is updated based on the computed pairwise frequencies.
        """
        pair_wise_frequencies = {}
        logging.info("ComputePairwiseFrequencies: Word '{}'...".format(word))
        # optimization: get articles that contain 'word'
        relevant_articles = data.loc[word,
                                     data.loc[data.index == word].any()]
        relevant_articles = relevant_articles.index.values.tolist()
        result = data.loc[:, relevant_articles].sum(axis=1)
        result.rename(word, inplace=True)
        result.to_sparse(fill_value=0)
        return result.astype(np.uint32, copy=False)

    def run(self):
        # |-------+-----------+-----------+----------|
        # | data  | Article 1 | Article 2 | Article3 |
        # |-------+-----------+-----------+----------|
        # | Word1 | True      | True      | True     |
        # | Word2 | True      | True      | True     |
        # | Word3 | False     | True      | False    |
        # |-------+-----------+-----------+----------|
        #
        # |--------+-------+-------+-------|
        # | result | Word1 | Word2 | Word3 |
        # |--------+-------+-------+-------|
        # | Word1  |     - | 3     |     1 |
        # | Word2  |     3 | -     |     1 |
        # | Word3  |     1 | 1     |     - |
        # |--------+-------+-------+-------|
        data = self.inputs
        result = Parallel(n_jobs=-1, verbose=1, backend='threading')(
            map(delayed(
                partial(ComputePairwiseFrequencies.compute_cooccurrence_frequencies,
                        data=data)),
                data.index.values.tolist()))

        logging.info("ComputePairwiseFrequencies: Finished computing pairwise frequencies. "
                     "Result size: {}".format(len(result)))
        logging.info("ComputePairwiseFrequencies: Building result data frame...")
        #result = [r.to_dense() for r in result]
        df = pd.concat(result, axis=1, copy=False)
        self.results = { "data" : df,
                         "collection_size": len(data.columns) }

    def write_results(self):
        file_name, _ = os.path.splitext(self.properties["input_file"])
        output_file_name = "{}_pairwire_frequencies.xz".format(file_name)
        logging.info("ComputePairwiseFrequencies: Writing results to file " \
                     "'{}'...".format(output_file_name))
        self.results["data"].to_pickle(output_file_name)

    def describe(self):
        return "Compute pairwise frequencies for the delta_p method"

class ComputeThetaPWeights(PipelineStage):
    def compute_presence_probabilities(self, data, frequency_matrix):
        # |--------+-------+-------+-------|
        # | data   | Word1 | Word2 | Word3 |
        # |--------+-------+-------+-------|
        # | Word1  | f_1   | 3     | 1     |
        # | Word2  | 3     |f_2    | 1     |
        # | Word3  | 1     | 1     | f_3   |
        # |--------+-------+-------+-------|
        #row i is p(w_i | w=present)
        # |--------+-------+-------+-------|
        # | result | Word1 | Word2 | Word3 |
        # |--------+-------+-------+-------|
        # | Word1  |   1   | 3/f_2 |1/f_3  |
        # | Word2  | 3/f_1 | 1     |1/f_3  |
        # | Word3  | 1/f_1 | 1/f_2 |1      |
        # |--------+-------+-------+-------|
        probability = data.divide(frequency_matrix, axis='index',
                                  level=None, fill_value=None)
        return probability

    def compute_absence_probabilities(self, data,
                                      frequency_matrix, collection_size):
        denom = frequency_matrix.rsub(collection_size, axis=0)
        assert(denom.where(denom >= 0).isnull().sum() == 0)
        num = data.rsub(frequency_matrix, axis='index', level= 0)
        #row i of probability == p(w_i | w=absent)
        probability = num.divide(denom)
        return probability

    def compute_delta_p(self, data, frequency_matrix, collection_size):
        # |--------+-------------+--------------+--------------|
        # | result | Word1       | Word2        | Word3        |
        # |--------+-------------+--------------+--------------|
        # | Word1  | 1           | delta_p(1,2) | delta_p(1,3) |
        # | Word2  | delta_p(2,1)|1             | delta_p(2,3) |
        # | Word3  | delta_p(3,1)| delta_p(3,2) |1             |
        # |--------+-------------+--------------+--------------|
        presence_prob = self.compute_presence_probabilities(data,
                                                            frequency_matrix)
        absence_prob = self.compute_absence_probabilities(data,
                                                          frequency_matrix,
                                                          collection_size)
        assert(presence_prob.shape == absence_prob.shape)
        delta_p = (absence_prob).rsub(presence_prob)
        return delta_p

    def compute_theta_p(self, data, frequency_matrix, collection_size):
        delta_p = self.compute_delta_p(data, frequency_matrix, collection_size)
        delta_p_transposed = delta_p.transpose()
        theta_p = delta_p_transposed.subtract(delta_p)
        return theta_p

    def run(self):
        data = self.inputs["data"]
        collection_size = self.inputs["collection_size"]
        frequency_matrix = pd.Series(np.diag(data), index=data.index)
        self.results = self.compute_theta_p(data, frequency_matrix,
                                            collection_size)

    def write_results(self):
        file_name, _ = os.path.splitext(self.properties["input_file"])
        output_file_name = "{}_theta_p_weights.xz".format(file_name)
        logging.info("ComputeThetaPWeights: Writing results to file " \
                     "'{}'...".format(output_file_name))
        self.results.to_pickle(output_file_name)

    def describe(self):
        return "Compute theta_p weights for the delta_p method"

class BuildGraphToolDataframe(PipelineStage):
    def run(self):
        df = self.inputs
        df = df.where(np.triu(np.ones(df.shape)).astype(bool)).stack().reset_index()
        df.columns = ["source","target","weight"]
        # filter out self links
        df = df.loc[df["source"] != df["target"],:]

        df_part_1 = df.loc[df["weight"] < 0]
        df_part_2 = df.loc[df["weight"] > 0]
        df_part_1 = df_part_1.rename(columns={"source":"target", "target":"source"})
        df_part_1 = df_part_1[["source", "target", "weight"]]
        df_part_1["weight"] = df_part_1["weight"].abs()
        df = pd.concat([df_part_1, df_part_2], sort=True)
        self.results = df

    def write_results(self):
        file_name, _ = os.path.splitext(self.properties["input_file"])
        output_file_name = "{}_graph_tool.xz".format(file_name)
        logging.info("BuildGraphToolDataframe: Writing results to file " \
                     "'{}'...".format(output_file_name))
        self.results.to_pickle(output_file_name)

    def describe(self):
        return "Build graph-tool dataframe"

class BuildGraphToolGraph(PipelineStage):
    def run(self):
        df = self.inputs
        g = Graph(directed=True)
        weights_prop = g.new_edge_property("float", val=0.0)
        g.add_edge_list(df.values, eprops=[weights_prop])
        g.edge_properties["weights"] = weights_prop
        self.results = g

    def write_results(self):
        file_name, _ = os.path.splitext(self.properties["input_file"])
        output_file_name = "{}_graph_tool_graph.gt".format(file_name)
        logging.info("BuildGraphToolGraph: Writing results to file " \
                     "'{}'...".format(output_file_name))
        self.results.save(output_file_name)
    def describe(self):
        return "Building graph-tool network"
