import logging
import time
from pipeline_stages import PipelineStage

class PipelineExecutor(object):
    def __init__(self, **kwargs):
        self.properties = kwargs
        self.stages = []

    def get_input_file(self):
        return self.properties["input_file"]

    def write_results(self):
        self.stages[-1].write_results()

    def is_debug(self):
        return self.properties["debug"]

    def add(self, stage):
        if not isinstance(stage, PipelineStage):
            logging.error("Successor is not a PipelineStage object")
            raise TypeError
        self.stages.append(stage)

    def connect(self):
        for (i, j) in zip(range(0, len(self.stages) - 1), range(1, len(self.stages))):
            self.stages[i].set_successor(self.stages[j])

    def run(self):
        self.connect()
        current_stage = 1
        sum_stages = len(self.stages)
        p_start = time.time()
        for s in self.stages:
            logging.info("Running stage {}/{}...".format(current_stage,
                                                         sum_stages))
            logging.info("{}: {}".format(s.__class__.__name__,
                                         s.describe()))
            bt = time.time()
            s.run()
            et = time.time()
            logging.info("Finished stage {}/{}: {} after {:.2f} " \
                         "seconds!".format(current_stage, sum_stages,
                                           s.describe(), et - bt))
            current_stage += 1
            if self.is_debug():
                logging.info("Writing results to disk...")
                s.write_results()
            s.set_outputs()
        logging.info("Pipeline finished after {:.2f} seconds!".format(
            time.time() - p_start))
        if not self.is_debug():
            logging.info("Writing pipeline results...")
            self.write_results()
